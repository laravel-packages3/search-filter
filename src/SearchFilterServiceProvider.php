<?php

namespace FoodNow\SearchFilter;

use Illuminate\Support\ServiceProvider;

class SearchFilterServiceProvider extends ServiceProvider
{
    public function register()
    {
        // Register your package's functionality here
    }

    public function boot()
    {
        $this->loadRoutesFrom(__DIR__ . '/routes/web.php');
        $this->loadViewsFrom(__DIR__ . '/views', 'search-filter');
        $this->loadMigrationsFrom(__DIR__ . '/database/migrations');
        $this->mergeConfigFrom(__DIR__ . '/config/search-filter.php', 'search-filter');
        $this->publishes([
            __DIR__.'/config/search-filter.php' => config_path('search-filter.php')
        ]);
    }
}
