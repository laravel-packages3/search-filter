<?php

use FoodNow\SearchFilter\Http\Controllers\SearchController;
use Illuminate\Support\Facades\Route;


Route::resource('search',SearchController::class);
